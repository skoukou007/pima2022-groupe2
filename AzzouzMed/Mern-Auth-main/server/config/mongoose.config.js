const mongoose = require("mongoose");

module.exports = () => {
  mongoose
    .connect(
      `mongodb+srv://pimadb:pimadb2023@cluster0.mni0gay.mongodb.net/test`,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      }
    )
    .then(() => console.log("connected to database"))
    .catch((err) =>
      console.log("can not connect to the database because ", err)
    );
};