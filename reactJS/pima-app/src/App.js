import logo from './logo.svg';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import React, { Component } from 'react';

import Statistics from './pima-modules/component/statistics/Statistics';
import Home from "./pima-modules/component/homepage";
import Explore from "./pima-modules/component/explore";
import ContactUs from "./pima-modules/component/contact_us";
import HelpCenter from "./pima-modules/component/helpCenter";
import HowItWorks from "./pima-modules/component/howItWorks";
import Legal from "./pima-modules/component/legal";
import Privacy from "./pima-modules/component/privacyPolicy";
import TermOfService from "./pima-modules/component/termOfService";
import Footer from "./pima-modules/component/footer";

import Dashboard from "./pima-modules/component/authenticationSystem/Dashboard";
import Register from "./pima-modules/component/authenticationSystem/Register";
import Login from "./pima-modules/component/authenticationSystem/Login";
import Header from "./pima-modules/component/header";
import {ThemeContext} from 'react';

import {
    BrowserRouter as Router,
    Routes,
    Route,
} from 'react-router-dom';

class App extends Component {
  constructor(props){
    super(props);
  }
  render(){
    return (<Router>
      <div className="App">
        <Header />
        <Routes>
          <Route exact path='' element={<Home/>}></Route>
          <Route exact path='/dashboard' element={<Dashboard />}></Route>
          <Route exact path="/explore" element={<Explore />}></Route>
          <Route exact path="/contact-us" element={<ContactUs />}></Route>
          <Route exact path="/help-center" element={<HelpCenter />}></Route>
          <Route exact path="/how-it-works" element={<HowItWorks />}></Route>
          <Route exact path="/legal" element={<Legal />}></Route>
          <Route exact path="/privacy-policy" element={<Privacy />}></Route>
          <Route exact path="/login" element={<Login />}></Route>
          <Route exact path="/register" element={<Register />}></Route>
          <Route
            exact
            path="/term-of-service"
            element={<TermOfService />}
          ></Route>
        </Routes>
        <div style={{ "minheight":"10vh" }} ></div>
        <div className="my-3"></div>
      </div>
      <Footer/>
      </Router>
    );
  }
}

export default App;
