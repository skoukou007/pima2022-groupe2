import React from "react";
import Home from "../components/homepage";
import Explore from "../components/explore";
import ContactUs from "../components/contact_us";
import HelpCenter from "../components/helpCenter";
import HowItWorks from "../components/howItWorks";
import Legal from "../components/legal";
import Privacy from "../components/privacyPolicy";
import TermOfService from "../components/termOfService";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
function Routers() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<Home />}></Route>
        <Route exact path="/explore" element={<Explore />}></Route>
        <Route exact path="/contact-us" element={<ContactUs />}></Route>
        <Route exact path="/help-center" element={<HelpCenter />}></Route>
        <Route exact path="/how-it-works" element={<HowItWorks />}></Route>
        <Route exact path="/legal" element={<Legal />}></Route>
        <Route exact path="/privacy" element={<Privacy />}></Route>
        <Route
          exact
          path="/term-of-service"
          element={<TermOfService />}
        ></Route>
      </Routes>
    </Router>
  );
}
export default Routers;
