<!DOCTYPE html>
<html>
<link rel="stylesheet" href="style.css" media="screen" type="text/css" />
<head> 
<meta charset="UTF-8">
</head>
<body>

<?php 

$API_Key    = 'AIzaSyAF5P3FWm83wwSJ5PCPE7ZLfmoRIzAic6c'; 
$Channel_ID = $_GET['ID']; 
$Max_Results = 20; 

$apiData_st = @file_get_contents('https://www.googleapis.com/youtube/v3/channels?part=statistics,snippet,brandingSettings&id='.$Channel_ID.'&key='.$API_Key.''); 
if($apiData_st){ 
    $channel_st = json_decode($apiData_st); 
}else{ 
    echo 'Invalid API key or channel ID.'; 
} 
if(!empty($channel_st->items)){ 
    foreach($channel_st->items as $item){
$channel_title = $item->snippet->title;

$followers = $item->statistics->subscriberCount;

$videos_nbr = $item->statistics->videoCount;

$views = $item->statistics->viewCount;

$average_views = floor($views/$videos_nbr);

$banner_img= $item->brandingSettings->image->bannerExternalUrl;

$profile_img = $item->snippet->thumbnails->default->url;

$channel_description = $item->snippet->description;

$channel_url = $item->snippet->customUrl;

$channel_creation = date("Y-m-d", strtotime($item->snippet->publishedAt));
    }
} 

$banner_alternative=0;

// Get videos from channel by YouTube Data API 
$apiData_v = @file_get_contents('https://www.googleapis.com/youtube/v3/search?order=date&channelId='.$Channel_ID.'&maxResults='.$Max_Results.'&key='.$API_Key."&part=snippet".''); 
if($apiData_v){ 
    $videoList = json_decode($apiData_v); 
}else{ 
    echo 'Invalid API key or channel ID.'; 
} 

$id_videos_array = array();
$title_array=array();
$likes_array=array();
$views_array=array();
$dates_array=array();
if(!empty($videoList->items)){ 
    foreach($videoList->items as $item){
        if(isset($item->id->videoId)){ 

            $id_videos_array = array_merge($id_videos_array,array($item->id->videoId));
            $title_array = array_merge($title_array,array($item->snippet->title));

            $video_id = $item->id->videoId;
            $apiData_video = @file_get_contents('https://www.googleapis.com/youtube/v3/videos?id='.$video_id.'&key='.$API_Key."&part=statistics,snippet".''); 
            if($apiData_video){ 
                $video_data = json_decode($apiData_video); 
            }else{ 
                echo 'Invalid API key or channel ID.'; 
            }
            if(!empty($video_data->items)){ 
                foreach($video_data->items as $item){
                    $banner_alternative=$item->snippet->thumbnails->maxres->url;

                    $likes_count = $item->statistics->likeCount;
                    $views_count = $item->statistics->viewCount;
                    $likes_array=array_merge($likes_array,array($likes_count));
                    $views_array=array_merge($views_array,array($views_count));
                }
            }

        } 
    }
}
$average_likes = floor(array_sum($likes_array)/$Max_Results);
?>

<h1 class="h1"> Welcome to the Youtube Channel of "<?php echo $channel_title; ?>" </h1>
<br>
<br>

<form action = "http://localhost:3000/">
<button class="back_button" type="submit">GO BACK</button>
    </form>

<?php
if(!isset($_POST['favorite']))
{
echo'
<form method="POST" action = "">
    <button class="favorite_button" type="submit" name="favorite"id="favorite">ADD TO FAVORITES</button>
</form>';
}
else
echo '<p class="favorite_button" id="favorite"> Added to favorite ! </p>';
?>
<?php
$banner_style= "
display: block;
margin-left: auto;
margin-right: auto;
width : 577px;
height : 280px;
";


$banner_img = '<img style="' . $banner_style . '" src= "' . $banner_img . '">';
$banner_alternative = '<img style="' . $banner_style . '" src= "' . $banner_alternative . '">';

$profile_style= "
position: absolute;
border-radius: 50%;
top: 355px;
left: 635px;
";
$profile_img = '<img style="' . $profile_style . '" src= "' . $profile_img . '">';
?>
<header>
<span class="cadre_description"> 
    <p> <span class="description_style"> Channel Custom URL </span> : <?php echo $channel_url;?> </p>
    <p>  <span class="description_style"> Channel Creation Date  </span> : <?php echo $channel_creation;?> </p>
    <p> <span class="description_style"> Channel Description </span> : <?php 
    $len = strlen($channel_description);
    if($len <= 599) {
        echo $channel_description;
    }
    else {
        echo substr($channel_description,0,599) . "...(To be Continued)";
    }
    ?> 
    </p>
</span>

<?php
if (!empty($banner_img)) {
echo $banner_alternative;
} else {
echo $banner_alternative; 
}
echo $profile_img;
echo '<br>';
?>
<h3 class='text_center'><?php echo $channel_title;?></h3>
<br>
<br>
<br>
<br>
</header>

<?php 
function thousand_format($number) { # Change the format of great numers to K, M, B...
    $number = (int) preg_replace('/[^0-9]/', '', $number);  
    if ($number >= 1000) {
        $rn = round($number);
        $format_number = number_format($rn);
        $ar_nbr = explode(',', $format_number);
        $x_parts = array('K', 'M', 'B', 'T', 'Q');
        $x_count_parts = count($ar_nbr) - 1;
        $dn = $ar_nbr[0] . ((int) $ar_nbr[1][0] !== 0 ? '.' . $ar_nbr[1][0] : '');
        $dn .= $x_parts[$x_count_parts - 1];

        return $dn;
    }
    return $number;
}

$followers=thousand_format($followers);
$videos_nbr=thousand_format($videos_nbr);
$views=thousand_format($views);
$average_views=thousand_format($average_views);
$average_likes=thousand_format($average_likes);
?>

<section>
    <article>
<div class='cadre_stats_1'>
    <div class='text_numbers'> <?php echo $videos_nbr?> </div>
    <br>
    <div class='text_titles'>MEDIA UPLOADS</div>
</div>

<div class='cadre_stats_2'>
    <div class='text_numbers'> <?php echo $followers ?> </div>
    <br>
    <div class='text_titles'>FOLLOWERS</div>
</div>

<div class='cadre_stats_3'>
    <div class='text_numbers'> <?php echo $views ?> </div>
    <br>
    <div class='text_titles'>TOTAL VIEWS</div>
</div>

<div class='cadre_stats_4'>
    <div class='text_numbers'> <?php echo $average_views ?> </div>
    <br>
    <div class='text_titles'>AVERAGE VIEWS</div>
</div>
<br>
<br>
<br>
<br>
<br>
<div class='cadre_stats_5'>
    <div class='text_numbers'> <?php echo $average_likes ?> </div>
    <br>
    <div class='text_titles'>AVERAGE LIKES</div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
</article>

<nav>
    <div class="text_center"><h2> Latest Youtube videos of "<span class="italic"><?php echo $channel_title; ?></span>" </h2>
</div>
<br>
<br>
<div class="div_center">

<div class="pos1_videos">
<?php
for($i=0;$i<floor($Max_Results/2);$i++) {
    $j=$i * 2;
    echo '
        <iframe width="480" height="300" src="https://www.youtube.com/embed/'.$id_videos_array[$j].'" frameborder="0" allowfullscreen></iframe> 
            <h4>'. $title_array[$j].'</h4>
            <h4> '. thousand_format($views_array[$j]) .'  VIEWS &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
            '. thousand_format($likes_array[$j]) .'  LIKES </h4>
            <br>
            <br>
            <br>';
}
?>
</div> 

<div class="pos2_videos">
<?php
for($i=0;$i<floor($Max_Results/2);$i++) {
    $j=(2* $i) + 1;
    echo '
        <iframe width="480" height="300" src="https://www.youtube.com/embed/'.$id_videos_array[$j].'" frameborder="0" allowfullscreen></iframe> 
            <h4>'. $title_array[$j].'</h4>
            <h4> '. thousand_format($views_array[$j]) .'  VIEWS &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
            '. thousand_format($likes_array[$j]) .'  LIKES </h4>
            <br>
            <br>
            <br>';
}

if(isset($_POST['favorite'])) {
    
}

?>
</div> 

</div>
</nav>




</body>
</html>
