import React, {useState} from 'react';
import './SearchBar.css';
import SearchIcon from '@mui/icons-material/Search';
import CloseIcon from '@mui/icons-material/Close';

function SearchBar({placeholder, data}) {
    /* searchbar */
    const [filteredData, setFilteredData] = useState([]);
    const [wordEntered, setWordEntered] = useState("");
    const handleFilter =(event) => {
        const searchWord = event.target.value;
        setWordEntered(searchWord);
        const newFilter = data.filter((value) => {
            return value.Name.toString().toLowerCase().includes(searchWord.toLowerCase());
        });
        if (searchWord === "") {
            setFilteredData([]);
        } else {
            setFilteredData(newFilter);
        }
    };

    const clearInput = () => {
        setFilteredData([]);
        setWordEntered("");
    };

    return (<div className="search">
            {/* Searchbar input */}
            <div className="searchInputs">
                <input
                    type = "text" 
                    placeholder={placeholder} 
                    value = {wordEntered} 
                    onChange={handleFilter}/>
                <div className = "searchIcon">
                    {filteredData.length === 0 ? (
                        <SearchIcon/>
                    ) : (
                        <CloseIcon id = "clearBtn" onClick = {clearInput}/>)} 
                </div>
            </div>
            {filteredData.length !== 0 && (
            <div className="dataResult position-absolute">
                {filteredData.slice(0, 20).map((value, key) => {
                    let isTiktok=value.hasOwnProperty("Tiktok Link");
                    let isInsta=value.hasOwnProperty("Link");
                    let isYoutube=!isTiktok&&!isInsta;
                    let suffix="";
                    let link="";
                    if(isTiktok) {suffix+=" (Tiktok)";link=value["Tiktok Link"]}
                    if(isInsta) {suffix+=" (Instagram)";link=value.Link;}
                    if(isYoutube) {suffix+=" (Youtube)";link="https://duong.iiens.net/laer/profil_page.php?ID="+value.ID;}
                    return (
                        <a className = "dataItem" href = {link}> 
                            <p>{value.Name + suffix}</p> 
                        </a>
                    );
                })}
            </div>
            )}
            </div>
    );
}
export default SearchBar