import React from 'react';
import Select from 'react-select';
import {components} from 'react-select';

/* Platform selection */
const platformOptions = [
    //{label: 'APL', value: 'allplatforms', image: 'https://iconarchive.com/download/i43605/treetog/junior/earth.ico'},
    {label: 'YTB', value: 'youtube', image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/YouTube_full-color_icon_%282017%29.svg/2560px-YouTube_full-color_icon_%282017%29.svg.png'},
    //{label: 'FB', value: 'facebook', image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Facebook-icon-1.png/640px-Facebook-icon-1.png'},
    {label: 'TK', value: 'tiktok', image: 'https://cdn.pixabay.com/photo/2021/06/15/12/28/tiktok-6338430_1280.png'},
    {label: 'IN', value: 'instagram', image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Instagram_icon.png/2048px-Instagram_icon.png'}
];

const { SingleValue, Option } = components;
const IconSingleValue = (props) => (
    <SingleValue {...props}>
        <img src={props.data.image} style={{ height: '30px', width: '30px', borderRadius: '50%', marginRight: '10px' }} alt=""/>
        {props.data.label}
    </SingleValue>
);
const IconOption = (props) => (
    <Option {...props} style={{pointerEvents: 'none'}}>
        <img src={props.data.image} style={{ height: '30px', width: '30px', borderRadius: '50%', marginRight: '10px' }} alt=""/>
        {props.data.label}
    </Option>
);
/*const customStyles = {
    option: (provided) => ({
        ...provided,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    }),
    singleValue: (provided) => ({
        ...provided,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    })
};*/

const fromStringToObj = (st) => {
    for(let platform of platformOptions)
    {
        if(platform.value===st)
            return platform;
    }
    return undefined;
};

const customStyles = {
    control: (base) => ({
      ...base,
      background: "black",
      borderColor: "black",
      borderRadius: 0,
      flex:1
    }),
};
const selectPlatform = (data) => (
    <Select 
        styles = {customStyles}
        components={{SingleValue: IconSingleValue, Option: IconOption}}
        defaultValue={data.values.map(fromStringToObj)}
        placeholder="Select social platforms..." 
        options={platformOptions}
        onChange={(choice) => data.callback(choice.values())}
        isMulti
    />
);

export default selectPlatform;
