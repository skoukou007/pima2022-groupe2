import React from "react";
import MostPopularPosts from './mostpopularPosts/MostPopularPosts';
import SearchBar from './searchbar/SearchBar';
import YoutubeData from './searchbar/youtube_channels.json';
import TiktokData from './searchbar/tiktok_influencers.json';
import InstaData from './searchbar/insta_influencers.json';
import Select from './searchbar/Select';
import { useSearchParams, useNavigate } from "react-router-dom";

function Home() {
    const [searchParams] = useSearchParams();
    const navigate = useNavigate();
    let putDefault=false;
    let inst=searchParams.get("instagram")!==null;
    let ytb=searchParams.get("youtube")!==null;
    let tktk=searchParams.get("tiktok")!==null;
    let allplatforms=searchParams.get("allplatforms")!==null;
    putDefault=!inst&&!ytb&&!tktk;
    let selectedMedia=[];
    if(putDefault)
      selectedMedia.push("youtube");
    else if(allplatforms)
      selectedMedia=["allplatforms"];
    else
      selectedMedia=[ytb?"youtube":"", inst?"instagram":"", tktk?"tiktok":""];

    let json=[];
    if(selectedMedia.some(media=>media==="youtube"))
      json=YoutubeData;
    if(selectedMedia.some(media=>media==="tiktok"))
      json=json.concat(TiktokData);
    if(selectedMedia.some(media=>media==="instagram"))
      json=json.concat(InstaData);

    return <span>
      <div className="container my-5">
        <div className="row">
          <div className="col-3">
            <Select values={selectedMedia} callback={x=>{
              let query="?";
              for(let media of x)
              {
                if(query!=="?")
                  query+="&";
                query+=media.value+"=show";
              }
              navigate("/"+query);
              navigate(0);
              }}/>
          </div>
          <div className="col-9">
            <SearchBar placeholder="Search an Influencer...Cristiano Ronaldo for example"
             data={json} />
          </div>
        </div>
      </div>
      <div className="my-5"></div>
        <MostPopularPosts media={selectedMedia} size={selectedMedia.length}/>
      </span>;
}

export default Home;