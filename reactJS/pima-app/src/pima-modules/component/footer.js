import React from "react";
import {
  MDBFooter,
  MDBContainer,
  MDBInput,
  MDBCol,
  MDBRow,
  MDBBtn,
  MDBNavbarBrand,
} from "mdb-react-ui-kit";
import {
  FaFacebookF,
  FaTwitter,
  FaInstagram,
  FaDiscord,
  FaConnectdevelop,
} from "react-icons/fa";
function Footer() {
  return (
    <MDBFooter className="text-center" color="white" bgColor="dark">
      <MDBContainer className="p-4">
        <section className="">
          <MDBRow>
            <MDBCol md="6" className="mb-4 mb-md-0">
              <form action="">
                <MDBRow>
                  <MDBCol md="6">
                    <MDBContainer fluid>
                      <MDBNavbarBrand href="/" className="link-light">
                        <FaConnectdevelop style={{ fontSize: "40px" }} />
                        Social Attack
                      </MDBNavbarBrand>
                    </MDBContainer>
                  </MDBCol>
                </MDBRow>
                <MDBRow className="d-flex justify-content-center">
                  <MDBCol size="auto">
                    <p className="pt-2">
                      <strong>Get the latest updates</strong>
                    </p>
                  </MDBCol>

                  <MDBCol md="5" start="12">
                    <MDBInput
                      contrast
                      type="email"
                      label="Email address"
                      className="mb-4"
                    />
                  </MDBCol>

                  <MDBCol size="auto">
                    <MDBBtn
                      outline
                      color="light"
                      type="submit"
                      className="mb-4 gradient-custom"
                    >
                      Email Me!
                    </MDBBtn>
                  </MDBCol>
                </MDBRow>
                <MDBRow>
                  <section className="mb-4">
                    <MDBBtn
                      outline
                      color="light"
                      floating
                      className="m-1"
                      href="https://www.facebook.com/"
                      role="button"
                    >
                      <FaFacebookF
                        style={{ margin: "5px", fontSize: "25px" }}
                      />
                    </MDBBtn>

                    <MDBBtn
                      outline
                      color="light"
                      floating
                      className="m-1"
                      href="https://twitter.com/"
                      role="button"
                    >
                      <FaTwitter style={{ margin: "5px", fontSize: "25px" }} />
                    </MDBBtn>

                    <MDBBtn
                      outline
                      color="light"
                      floating
                      className="m-1"
                      href="https://www.instagram.com/"
                      role="button"
                    >
                      <FaInstagram
                        style={{ margin: "5px", fontSize: "25px" }}
                      />
                    </MDBBtn>

                    <MDBBtn
                      outline
                      color="light"
                      floating
                      className="m-1"
                      href="https://discord.com/"
                      role="button"
                    >
                      <FaDiscord style={{ margin: "5px", fontSize: "25px" }} />
                    </MDBBtn>
                  </section>
                </MDBRow>
              </form>
            </MDBCol>

            <MDBCol md="3" className="mb-4 mb-md-0">
              <h5 className="text-uppercase">Social Attack</h5>

              <ul className="list-unstyled mb-0">
                <li>
                  <a href="/explore" className="text-white">
                    Explore
                  </a>
                </li>
                <li>
                  <a href="/how-it-works" className="text-white">
                    How it works
                  </a>
                </li>
                <li>
                  <a href="/contact-us" className="text-white">
                    Contact Us
                  </a>
                </li>
              </ul>
            </MDBCol>

            <MDBCol md="3" className="mb-4 mb-md-0">
              <h5 className="text-uppercase">Support</h5>

              <ul className="list-unstyled mb-0">
                <li>
                  <a href="/help-center" className="text-white">
                    Help center
                  </a>
                </li>
                <li>
                  <a href="/term-of-service" className="text-white">
                    Term of service
                  </a>
                </li>
                <li>
                  <a href="/legal" className="text-white">
                    Legal
                  </a>
                </li>
                <li>
                  <a href="/privacy-policy" className="text-white">
                    Privacy policy
                  </a>
                </li>
              </ul>
            </MDBCol>
          </MDBRow>
        </section>
      </MDBContainer>
    </MDBFooter>
  );
}

export default Footer;
