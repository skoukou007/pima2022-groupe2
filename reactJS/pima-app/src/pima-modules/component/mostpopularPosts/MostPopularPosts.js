//import ytbrawdata from '../../api/youtube-api/youtubeTempJSON'; //-> needs to be fetched by an HTTP request with the API key -> DONE
//import tktkrawdata from '../../api/tiktok-api/tiktokTempJson'; //-> needs to be fetched by an HTTP request with the API key
import React, { Component } from 'react';
import ytbLogo from './yt_logo_rgb_light.png';
import tklogo from './tiktok.png';
import inlogo from './instagram.png';
import { format } from 'date-fns';

/** Props needs the following attributes : 
 ** thumbnail: URL to the video thumbnail
 ** title: Title of the video
 ** description: Description of the video
 ** id: ID of the video
 ** views: View count of the video
 ** rank: Ranking on the trending chart
 */
function YoutubePost(props)
{
	return <div className="card text-white bg-dark mb-5 h-100">
			  <div className="card-body pb-0">
			    <img className="img-fluid" src={props.thumbnail} alt={"Image of the video "+props.title}/>
			    <h5 className="card-title text-left">{props.title}</h5>
			    <a target="_blank" rel="noreferrer" href={"https://www.youtube.com/watch?v="+props.id}
			     className="stretched-link"> </a>
			  </div>
			  <div className="card-footer pt-0">
				  <div className="container-fluid">
				  <div className="row">
					  <div className="col-1 mx-0 px-0">
					  	<img className="img-fluid rounded" src={ytbLogo} alt="Youtube Logo"/>
					  </div>
					  <div className="col-4">
					  	#{props.rank}
					  </div>
					  <div className="text-right col-7">
					    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-eye" viewBox="0 0 16 16">
							  <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"/>
							  <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
						</svg>
						{props.views}
					  </div>
				  </div>
				  </div>
			  </div>
			</div>;
}

let MAX_POSTS_ON_SCREEN=8;

/** Props needs an attribute called data which is an array of the youtube videos.
 ** It's usually in the 'items' property of Youtube API JSON response. Check Youtube API docs for more infos.
 */
function YoutubePosts(props)
{
	const date=new Date();
	const st=format(date, 'yyyy/MM/dd kk:mm:ss');
	if(props.data.length<=0)
		return (<div className="container">
			<div className="row h3 text-white">Most Popular Posts of the day - Youtube</div>
				<div className="row mb-0 pb-0">
					<div className="col-3 my-3 pb-0 mr-1">
						<div className="row h3 text-white">
							<span className="card bg-dark mb-5 h-100">
								Could not fetch the data
							</span>
						</div>
				</div>
			</div>
		</div>);
	else
	 return (<div className="container">
	 			<div className="row h3 text-white">Most Popular Posts of the day - Youtube - Updated on {st}</div>
				<div className="row mb-0 pb-0">
					{props.data.slice(0,MAX_POSTS_ON_SCREEN).map((x, i) => (
						<div className="col-3 my-3 pb-0 mr-1" key={i}>
							<YoutubePost title={x.snippet.title} description={x.snippet.description}
										 rank={i+1} thumbnail={x.snippet.thumbnails.medium.url}
										 views={x.statistics.viewCount} id={x.id}/>
						</div>)
					)}
				</div>
			</div>);
};

function TiktokPost(props)
{
	return <div className="card text-white bg-dark mb-5 h-100">
			  <div className="card-body pb-0">
			    <img className="img-fluid" src={props.thumbnail} alt={"Image of the video "+props.description}/>
			    <h5 className="card-title text-left">{props.description+" - "+props.title}</h5>
			    <a target="_blank" rel="noreferrer" href={props.id}
			     className="stretched-link"> </a>
			  </div>
			  <div className="card-footer pt-0">
				  <div className="container-fluid">
				  <div className="row">
					  <div className="col-1 mx-0 px-0">
					  	<img className="img-fluid rounded" src={tklogo} alt="TikTok Logo"/>
					  </div>
					  <div className="col-4">
					  	#{props.rank}
					  </div>
					  <div className="text-right col">
					    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-heart" viewBox="0 0 16 16">
						  <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
						</svg>
						{props.views}
					  </div>
				  </div>
				  </div>
			  </div>
			</div>;
}

function TiktokPosts(props)
{
	 return (<div className="container">
	 			<div className="row h3 text-white">Most Popular Posts of the day - TikTok</div>
				<div className="row mb-0 pb-0">
					{props.data.slice(0,MAX_POSTS_ON_SCREEN).map((x, i) => (
						<div className="col-3 my-3 pb-0 mr-1" key={i}>
							<TiktokPost title={x.author.nickname} description={x.desc}
										 rank={i+1} thumbnail={x.author.cover_url[0].url_list[0]}
										 views={x.statistics.digg_count} id={x.share_url}/>
						</div>)
					)}
				</div>
			</div>);
};

function importAll(r) {
	let images = {};
	r.keys().map((item, index) => { images[item.replace('./', '')] = r(item); });
	return images;
  }

function InstagramPost(props)
{
	const images = importAll(require.context('./images', false, /\.(png|jpe?g|svg)$/));
	return <div className="card text-white bg-dark mb-5 h-100">
			  <div className="card-body pb-0">
			    <img className="img-fluid" src={images[props.thumbnail]} alt={"Photo "+props.thumbnail}/>
			    <h5 className="card-title text-left">{props.description+" - "+props.title}</h5>
			    <a target="_blank" rel="noreferrer" href={props.id}
			     className="stretched-link"> </a>
			  </div>
			  <div className="card-footer pt-0">
				  <div className="container-fluid">
				  <div className="row">
					  <div className="col-1 mx-0 px-0">
					  	<img className="img-fluid rounded" src={inlogo} alt="TikTok Logo"/>
					  </div>
					  <div className="col-4">
					  	#{props.rank}
					  </div>
					  <div className="text-right col">
					    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-heart" viewBox="0 0 16 16">
						  <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
						</svg>
						{props.views}
					  </div>
				  </div>
				  </div>
			  </div>
			</div>;
}

function InstagramPosts(props)
{
	 return (<div className="container">
	 			<div className="row h3 text-white">Most Popular Posts of the day - Instagram</div>
				<div className="row mb-0 pb-0">
					{props.data.slice(0,MAX_POSTS_ON_SCREEN).map((x, i) => (
						<div className="col-3 my-3 pb-0 mr-1" key={i}>
							<InstagramPost title={x.caption} description={""}
										 rank={i+1} thumbnail={x.url}
										 views={x.likes} id={""}/>
						</div>)
					)}
				</div>
			</div>);
};

class MostPopularPosts extends Component
{
  constructor (props) {
	super(props);
    this.state = {media:props.media, mediasize:props.size};
  }
   MAX=(a,b)=>a<b?b:a;
   MIN=(a,b)=>a>b?b:a;
   isJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
	}

  componentDidMount () {
	let ytbrawdata = "https://duong.iiens.net/postsApi.php"
	fetch(ytbrawdata)
		.then(r => r.text())
		.then(text=>{
			let ytboutputdata=[];
			if(!this.isJsonString(text))
			{
				this.setState({ytboutputdata});
				return;
			}
		
			let data=JSON.parse(text).items;
			for(let d of data)
			{
				ytboutputdata.push(d);
			}
			this.setState({ytboutputdata});
		});
	let tktkrawdata = "https://duong.iiens.net/postsApi.php?media=tiktok";
	fetch(tktkrawdata)
		.then(r => r.text())
		.then(text=>{
			let data=JSON.parse(text).aweme_list;
			let tkoutputdata=[];
			for(let d of data)
			{
				tkoutputdata.push(d);
			}
			this.setState({tkoutputdata});
		});
	let instadata="https://duong.iiens.net/postsApi.php?media=instagram&tag=clothes";
	fetch(instadata)
		.then(r => r.text())
		.then(text=>{
			let instaoutputdata=[];
			if(!this.isJsonString(text))
			{
				this.setState({instaoutputdata});
				return;
			}
		
			let data=JSON.parse(text);
			for(let d of data)
			{
				instaoutputdata.push(d);
			}
			this.setState({instaoutputdata});
		});
  }

	render()
	{
		let showyoutube=false;
		let showtiktok=false;
		let showinsta=false;
		if(this.state.media===undefined)
		{
			showyoutube=true;
			showtiktok=true;
			showinsta=true;
		}
		else
			for(let media of this.state.media)
			{
				if(media==="youtube")
					showyoutube=true;
				if(media==="tiktok")
					showtiktok=true;
				if(media==="instagram")
					showinsta=true;
				if(media==="allplatforms")
				{
					showtiktok=true;
					showyoutube=true;
				}
			}

		//const nmedia=showyoutube+showtiktok;
		//const colsize=this.MIN(this.MAX(12-6*(1-nmedia), 6), 6);
		const colsize=12;

		return ((this.state.ytboutputdata&&this.state.tkoutputdata) &&([
			<div className="container"><div className="row">,
				{showyoutube ? <div className={"col-"+colsize}><YoutubePosts data={this.state.ytboutputdata} /></div>:null}
				{showtiktok ? <div className={"col-"+colsize}><TiktokPosts data={this.state.tkoutputdata} /></div>:null}
				{showinsta ? <div className={"col-"+colsize}><InstagramPosts data={this.state.instaoutputdata} /></div>:null}
			</div></div>]
		))
		||
		((!this.state.ytboutputdata&&!this.state.tkoutputdata) && [<div className="text-white h1">Loading....</div>]);
	}
}

export default MostPopularPosts;