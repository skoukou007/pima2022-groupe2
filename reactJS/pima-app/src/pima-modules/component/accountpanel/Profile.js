import React from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardHeader,
  MDBTabs,
  MDBTabsItem,
  MDBTabsLink,
  MDBContainer,
  MDBTable,
  MDBTableBody,
} from "mdb-react-ui-kit";

export default function Profile({username,lastname,firstname,email,logout}) {
  return (
    <MDBContainer className="d-flex justify-content-center">
      <MDBCard className="w-75">
        <MDBCardHeader>
          <MDBTabs className="card-header-tabs">
            <MDBTabsItem>
              <MDBTabsLink active>Account overview</MDBTabsLink>
            </MDBTabsItem>
            <MDBTabsItem>
              <MDBTabsLink>Edit profile</MDBTabsLink>
            </MDBTabsItem>
            <MDBTabsItem>
              <MDBTabsLink>Change password</MDBTabsLink>
            </MDBTabsItem>
            <MDBTabsItem>
              <MDBTabsLink onClick={logout}>Disconnect</MDBTabsLink>
            </MDBTabsItem>
          </MDBTabs>
        </MDBCardHeader>
        <MDBCardBody>
          <MDBCardTitle>Profile</MDBCardTitle>
          <MDBTable>
            <MDBTableBody>
              <tr>
                <th scope="row"></th>
                <td colSpan={2}>
                  <div className="">
                    <p className="text-muted">Username</p>
                  </div>
                </td>
                <td>
                  <div className="">
                    <p className="fw-bold ">{username}</p>
                  </div>
                </td>
              </tr>
              <tr>
                <th scope="row"></th>
                <td colSpan={2}>
                  <div className="">
                    <p className="text-muted">First Name</p>
                  </div>
                </td>
                <td>
                  <div className="">
                    <p className="fw-bold ">{firstname}</p>
                  </div>
                </td>
              </tr>
              <tr>
                <th scope="row"></th>
                <td colSpan={2}>
                  <div className="">
                    <p className="text-muted">Last Name</p>
                  </div>
                </td>
                <td>
                  <div className="">
                    <p className="fw-bold ">{lastname}</p>
                  </div>
                </td>
              </tr>
              <tr>
                <th scope="row"></th>
                <td colSpan={2}>
                  <div className="">
                    <p className="text-muted">Email</p>
                  </div>
                </td>
                <td>
                  <div className="">
                    <p className="fw-bold ">{email}</p>
                  </div>
                </td>
              </tr>
            </MDBTableBody>
          </MDBTable>
        </MDBCardBody>
        <MDBCardBody>
        <MDBCardTitle>Favourites</MDBCardTitle>
        <MDBTable>
            <MDBTableBody>
              <tr>
                <th scope="row"></th>
                <td colSpan={2}>
                  <div className="">
                    <p className="text-muted">Squeezie (Youtube)</p>
                  </div>
                </td>
                <td>
                  <div className="">
                    <p className="fw-bold "><a href="https://duong.iiens.net/laer/profil_page.php?ID=UCWeg2Pkate69NFdBeuRFTAw">Link</a></p>
                  </div>
                </td>
              </tr>
              <tr>
                <th scope="row"></th>
                <td colSpan={2}>
                  <div className="">
                    <p className="text-muted">Ninja (Youtube)</p>
                  </div>
                </td>
                <td>
                  <div className="">
                    <p className="fw-bold "><a href="https://duong.iiens.net/laer/profil_page.php?ID=UCAW-NpUFkMyCNrvRSSGIvDQ">Link</a></p>
                  </div>
                </td>
              </tr>
              <tr>
                <th scope="row"></th>
                <td colSpan={2}>
                  <div className="">
                    <p className="text-muted">PewDiePie (Youtube)</p>
                  </div>
                </td>
                <td>
                  <div className="">
                    <p className="fw-bold "><a href="https://duong.iiens.net/laer/profil_page.php?ID=UC-lHJZR3Gqxm24_Vd_AJ5Yw">Link</a></p>
                  </div>
                </td>
              </tr>
            </MDBTableBody>
          </MDBTable>
        </MDBCardBody>
      </MDBCard>
    </MDBContainer>
  );
}
