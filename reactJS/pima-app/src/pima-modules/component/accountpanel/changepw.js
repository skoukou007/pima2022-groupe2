import React from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardHeader,
  MDBTabs,
  MDBTabsItem,
  MDBTabsLink,
  MDBContainer,
  MDBBtn,
  MDBInput,
} from "mdb-react-ui-kit";

export default function App() {
  return (
    <MDBContainer className="d-flex justify-content-center">
      <MDBCard className="w-75">
        <MDBCardHeader>
          <MDBTabs className="card-header-tabs">
            <MDBTabsItem>
              <MDBTabsLink>Account overview</MDBTabsLink>
            </MDBTabsItem>
            <MDBTabsItem>
              <MDBTabsLink>Edit profile</MDBTabsLink>
            </MDBTabsItem>
            <MDBTabsItem>
              <MDBTabsLink active>Change password</MDBTabsLink>
            </MDBTabsItem>
          </MDBTabs>
        </MDBCardHeader>
        <MDBCardBody>
          <MDBCardTitle>Change new password</MDBCardTitle>
          <MDBContainer className="d-flex justify-content-center">
            <form>
              <MDBInput className="mb-4" type="password" label="New password" />
              <MDBInput
                className="mb-4"
                type="password"
                label="Confirm password"
              />

              <MDBBtn type="submit" block color="dark" rippleColor="dark">
                Submit
              </MDBBtn>
            </form>
          </MDBContainer>
        </MDBCardBody>
      </MDBCard>
    </MDBContainer>
  );
}
