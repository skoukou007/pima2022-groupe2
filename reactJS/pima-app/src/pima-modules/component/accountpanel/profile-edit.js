import React from "react";
import {
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardHeader,
  MDBTabs,
  MDBTabsItem,
  MDBTabsLink,
  MDBContainer,
  MDBTable,
  MDBTableBody,
  MDBInput,
  MDBBtn,
} from "mdb-react-ui-kit";

export default function App() {
  return (
    <MDBContainer className="d-flex justify-content-center">
      <MDBCard className="w-75">
        <MDBCardHeader>
          <MDBTabs className="card-header-tabs">
            <MDBTabsItem>
              <MDBTabsLink>Account overview</MDBTabsLink>
            </MDBTabsItem>
            <MDBTabsItem>
              <MDBTabsLink active>Edit profile</MDBTabsLink>
            </MDBTabsItem>
            <MDBTabsItem>
              <MDBTabsLink>Change password</MDBTabsLink>
            </MDBTabsItem>
          </MDBTabs>
        </MDBCardHeader>
        <form>
          <MDBCardBody>
            <MDBCardTitle>Profile</MDBCardTitle>
            <MDBTable>
              <MDBTableBody>
                <tr>
                  <th scope="row"></th>
                  <td colSpan={2}>
                    <div className="">
                      <p className="text-muted">Username</p>
                    </div>
                  </td>
                  <td>
                    <div className="w-50">
                      <MDBInput
                        className="form-control"
                        type="text"
                        placeholder="Jackie"
                      />
                    </div>
                  </td>
                </tr>
                <tr>
                  <th scope="row"></th>
                  <td colSpan={2}>
                    <div className="">
                      <p className="text-muted">First Name</p>
                    </div>
                  </td>
                  <td>
                    <div className="w-50">
                      <MDBInput
                        className="form-control"
                        type="text"
                        placeholder="Jack"
                      />
                    </div>
                  </td>
                </tr>
                <tr>
                  <th scope="row"></th>
                  <td colSpan={2}>
                    <div className="">
                      <p className="text-muted">Last Name</p>
                    </div>
                  </td>
                  <td>
                    <div className="w-50">
                      <MDBInput
                        className="form-control"
                        type="text"
                        placeholder="Daniel"
                      />
                    </div>
                  </td>
                </tr>
                <tr>
                  <th scope="row"></th>
                  <td colSpan={2}>
                    <div className="">
                      <p className="text-muted">Email</p>
                    </div>
                  </td>
                  <td>
                    <div className="w-50">
                      <MDBInput
                        className="form-control"
                        type="text"
                        placeholder="daniel.jk@gmail.com"
                      />
                    </div>
                  </td>
                </tr>
              </MDBTableBody>
            </MDBTable>
            <MDBBtn color="dark">Save</MDBBtn>
          </MDBCardBody>
        </form>
      </MDBCard>
    </MDBContainer>
  );
}
