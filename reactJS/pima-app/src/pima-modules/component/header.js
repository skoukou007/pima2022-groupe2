import React, { useState } from "react";
import {
  MDBNavbar,
  MDBContainer,
  MDBNavbarNav,
  MDBNavbarItem,
  MDBNavbarLink,
  MDBNavbarToggler,
  MDBNavbarBrand,
  MDBCollapse,
  MDBBtn,
} from "mdb-react-ui-kit";
import { FaConnectdevelop } from "react-icons/fa";
export default function App() {
  const [showNavColor, setShowNavColor] = useState(false);

  return (
    <>
      <MDBNavbar className="container-fluid" expand="lg" dark bgColor="dark">
        <MDBContainer fluid>
          <MDBNavbarBrand href="/" className="link-light">
            <FaConnectdevelop style={{ fontSize: "40px" }} />
            Social Attack
          </MDBNavbarBrand>
          <MDBNavbarToggler
            type="button"
            data-target="#navbarColor02"
            aria-controls="navbarColor02"
            aria-expanded="false"
            aria-label="Toggle navigation"
            onClick={() => setShowNavColor(!showNavColor)}
          ></MDBNavbarToggler>

          <MDBCollapse show={showNavColor} navbar id="navbarColor02">
            <MDBNavbarNav className="me-auto mb-2 mb-lg-0">
              <MDBNavbarItem>
                <MDBNavbarLink href="./explore">Explore</MDBNavbarLink>
              </MDBNavbarItem>
              <MDBNavbarItem>
                <MDBNavbarLink href="./how-it-works">How it works</MDBNavbarLink>
              </MDBNavbarItem>
            </MDBNavbarNav>
          </MDBCollapse>
            <>
              <MDBNavbarLink href="/login">
                <MDBBtn outline color="light">
                Login
                </MDBBtn>
              </MDBNavbarLink>
              <MDBNavbarLink href="/register">
                <MDBBtn outline color="light">
                Register
                </MDBBtn>
              </MDBNavbarLink>
            </>
        </MDBContainer>
      </MDBNavbar>
    </>
  );
}
