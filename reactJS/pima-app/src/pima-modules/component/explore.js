import React from "react";
import im1 from "./marketing.jpg";

function Explore() {
  return [<h1 className="text-white">Explore</h1>,
  <p className="text-white fs-5 text-justify text-monospace">Select your targeted media and 
      search for the right influencers for your projects.</p>,
  <div className="container-sm text-white">
    <div className="row">
      <div className="col-7">
        <img src={im1} className="img-fluid img"/>
      </div>
      <div className="col-5 my-5">
        <p className="h3 text-decoration-underline">The right place to find your collaborators</p>
        <p className="h4 lh-base">This application helps you in your search for the best influencers who can help you advertise your products.</p>
      </div>
    </div>
  </div>
  ];
}
export default Explore;
