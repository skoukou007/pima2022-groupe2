import style from './Statistics.css';
import ronaldo from './ronaldo.jpg';
import insta from './instagram.webp';
import facebook from './facebook.png';
import twitter from './twitter.jpg';

function Statistics()
{
	return (<div>
	<div id="formContent">
		<div align="center" id="containerindex">
						<h1>Social Media Statistics</h1>
						</div>
			  </div>
			  <br/>
			  <div id="formContent">
				<div id="containerindex"> <h3>CRISTIANO RONALDO</h3></div>
			</div>

			<br/>
			<br/>
			
			  <table cellspacing="1" width="450" align="center">
				<tr>
					<td><img class="imglaer" src={ronaldo} alt="ronaldo"/></td>
					<td className={style.entete}><img class="imglaer" src={insta} alt="Instagram"/></td>
					<td className={style.entete}><img class="imglaer" src={facebook} alt="facebook"/></td>
					<td className={style.entete}><img class="imglaer" src={twitter} alt="twitter"/></td>
				</tr>
				<tr>
					<td>FOLLOWERS</td>
					<td>485M</td>
					<td>103,8M</td>
					<td>153M</td>
				</tr>
				<tr>
					<td>AVERAGE LIKES</td>
					<td>6M</td>
					<td>2M</td>
					<td>400K</td>
				</tr>
				<tr>
					<td>ESTIMATED EARNINGS</td>
					<td>$27K USD - $329K USD</td>
					<td>UNKOWN</td>
					<td>$800K USD</td>
				</tr>
			 </table>
			</div>
	);
}

export default Statistics;