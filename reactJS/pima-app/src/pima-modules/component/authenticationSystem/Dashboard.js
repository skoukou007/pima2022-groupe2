import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import Profile from "../accountpanel/Profile";

const Dashboard = () => {
  const navigate = useNavigate();
  const [loggedinuser, setLoggedInUser] = useState({});
  
  useEffect(() => {
    axios
      .get("http://localhost:8000/api/users/getloggedinuser", {
        withCredentials: true,
      })
      .then((res) => {
        console.log("logged in user info", res);
        setLoggedInUser(res.data);
      })
      .catch((err) => {
        navigate("/");
        console.log("error", err);
      });
  }, []);

  const logout = (e) => {
    e.preventDefault();
    axios.get("http://localhost:8000/api/logout").then(() => {
      console.log("you re out of here");
      navigate("/login");
    });
  };
  return (
    <div>
      <h1 className="text-white">Welcome, {loggedinuser.username} you made it to the dashboard!</h1>
      <Profile username={loggedinuser.username}
      firstname={loggedinuser.firstname}
      lastname={loggedinuser.lastname}
      email={loggedinuser.email}
      logout={logout}/>
    </div>
  );
};

export default Dashboard;
