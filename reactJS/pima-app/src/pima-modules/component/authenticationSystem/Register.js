import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const Register = () => {
  const history = useNavigate();
  const [registerInfo, setRegisterInfo] = useState({
    username: "",
    email: "",
    password: "",
    confirm: "",
  });

  const [errors, setErrors] = useState({
    username: "",
    email: "",
    password: "",
    confirm: "",
  });

  const regChangeHandler = (e) => {
    setRegisterInfo({
      ...registerInfo,
      [e.target.name]: e.target.value,
    });
  };

  const register = (e) => {
    e.preventDefault();
    axios
      .post("http://localhost:8000/api/register", registerInfo, {
        withCredentials: true,
      })
      .then((res) => {
        console.log("response from registering", res);
        if (res.data.errors) {
          setErrors(res.data.errors);
        } else {
          console.log("success!");
          history("/login");
        }
      })
      .catch((err) => console.log(err));
  };

  return (
    <>
      <div className="Auth-form-container">
      <form className="Auth-form" onSubmit={register}>
        <div className="Auth-form-content">
          <h3 className="Auth-form-title">Sign In</h3>
          <div className="text-center">
            Already registered?{" "}
            <span className="link-primary" >
              Sign In
            </span>
          </div>
          <div className="form-group mt-3">
            <label>Full Name</label>
            <input
              onChange={regChangeHandler}
              type="text"
              className="form-control mt-1"
              placeholder="e.g Jane Doe"
              name="username"
            />
             {errors.username ? (<p className="text-danger">{errors.username.message}</p>) : ("")}
          </div>
          <div className="form-group mt-3">
            <label>Email address</label>
            <input
              type="email"
              className="form-control mt-1"
              placeholder="Email Address"
              name="email"
              onChange={regChangeHandler}
            />
            {errors.email ? (
              <p className="text-danger">{errors.email.message}</p>
            ) : (
              ""
            )}
          </div>
          <div className="form-group mt-3">
            <label>Password</label>
            <input
              type="password"
              onChange={regChangeHandler}
              name="password"
              className="form-control mt-1"
              placeholder="Password"
            />
              {errors.password ? (
              <p className="text-danger">{errors.password.message}</p>
            ) : (
              ""
            )}
          </div>
          <div className="form-group mt-3">
            <label>Confirm Password</label>
            <input
              type="password"
              onChange={regChangeHandler}
              name="confirm"
              className="form-control mt-1"
              placeholder="Password"
            />
              {errors.confirm ? (
              <p className="text-danger">{errors.confirm.message}</p>
            ) : (
              ""
            )}
          </div>
          <div className="d-grid gap-2 mt-3">
            <button type="submit" value="Sign Up" className="btn btn-primary">
              Submit
            </button>
          </div>
          <p className="text-center mt-2">
            Forgot <a href="#">password?</a>
          </p>
        </div>
      </form>
    </div>

    </>
  );
};

export default Register;
