import os
import json

hashtag="clothes"
dossier="./#"+hashtag
res=[]
for file in os.listdir(dossier):
    name=os.path.splitext(file)[0]
    extension=os.path.splitext(file)[1]
    if extension==".json":
        if name=="#"+hashtag:
            continue
        else:
            jsonlu=json.load(open(dossier+"/"+file))
            if 'node' not in jsonlu:
                continue
            caption=jsonlu['node']['caption'][:30]
            url=name+"_1.jpg"
            likes=jsonlu['node']['iphone_struct']['like_count']
            commentaires=jsonlu['node']['iphone_struct']['comment_count']
            
            res.append({"caption":caption, "likes":likes, "commentaires":commentaires,"url":url})

with open(dossier+"/res.json", "w") as outfile:
    json.dump(res, outfile)